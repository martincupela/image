from .bmp import BmpImage, BmpPixelMap, BmpPixelRow
from .base import Image, PixelMap, PixelRow, Pixel
from .exceptions import FileNameError

SUPPORTED_FORMATS = {
    'bmp': {
        'Signature': b'BM',
        'class': BmpImage
        }
    }


def open_img(filepath, f_mode='rb', width=50, height=50, depth=24):
    """Opens an existing file, extracts header fields information
    and stores them in the new Image object
    
    :param filepath: path to the image file
    :type filepath: string
    :f_mode: string representing file object mode (rb, wb, ab etc.)
    :type f_mode: string

    :returns: Image object
    """
    try:
        with open(filepath, f_mode) as file:
            data = file.read()
            file_format = _read_format(data)
            if not file_format in SUPPORTED_FORMATS:
                raise ValueError('Unsupported file format: "{}"'.format(file_format))

            img_cls = SUPPORTED_FORMATS[file_format]['class']
            new = img_cls(filepath=filepath, data=data)
    
    except FileNotFoundError:
        file_format = get_format_from_name(filepath)
        if not file_format in SUPPORTED_FORMATS:
                raise ValueError('Unsupported file format: "{}"'.format(file_format))

        img_cls = SUPPORTED_FORMATS[file_format]['class']
        kwargs = {
            'filepath': filepath,
            'size': (width, height),
            'depth': depth
        }
        new = img_cls(**kwargs)

    return new

def _read_format(data):
    '''Extract the file format from image bytes'''
    if data:
        for marker, spec in SUPPORTED_FORMATS.items():
            signature_start = 0
            signature_end = len(spec['Signature'])

            if data[signature_start: signature_end] == spec['Signature']:
                return marker

    raise UnsupportedFormatError(
        'Cannot recognize the file format')

def get_format_from_name(name):
    ext = name.rsplit('.')[-1].lower()
    if ext not in SUPPORTED_FORMATS:
        raise FileNameError('Unsupported file extension: {}'.format(ext))
    return ext