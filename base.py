import os
from datetime import datetime as dt

from .utils import *

class Pixel:
    
    BLACK = 0
    WHITE = 255

    def __init__(self, color, format='bmp'):
        """Create an instance representing image pixel.
        
        :param color: tuple or list of 8bit integers or a bytes object
        :type color: tuple / list / bytes

        :returns: Pixel object
        """
        if isinstance(color, bytes):
            pixel = color
        elif isinstance(color, (tuple, list)):
            pixel = b''

            if format=='bmp':
                color = reversed(color)

            for channel in color:
                pixel += bin8(channel)
        else:
            raise ValueError('Expected bytes, list or tuple of ints')
            
        self._pixel = pixel

    def __repr__(self):
        return ('<{}>: {}'.format(self.__class__.__name__, self._pixel))

    def __bytes__(self):
        return self._pixel

    def __iter__(self):
        for b in bytes(self):
            yield int(b)

    def __len__(self):
        return len(self._pixel)

    def __eq__(self, other):
        if isinstance(other, Pixel):
            return self._pixel == other._pixel
        else:
            return False

    def __add__(self, other):
        if isinstance(other, Pixel):
            pm = PixelRow(pixels=[self, other])
            return pm
        raise ValueError('Expected Pixel instance')


class PixelRow:

    PIXEL_CLS = Pixel

    def __init__(self, pixels=[]):
        """Create an instance representing a 1-dimensional
        matrix of pixels.
        
        :param paramixels: path to the image file
        :type pixels: iterable of Pixel objects

        :returns: PixelRow object
        """

        if not hasattr(pixels, '__iter__'):
            raise ValueError('pixels argument has to be an iterable')
        self._row = []
        for item in pixels:
            if not isinstance(item, self.PIXEL_CLS):
                self._row.append(self.PIXEL_CLS(item))
            else:
                self._row.append(item)

    def __repr__(self):
        return "{} {}".format(self.__class__.__name__, self._row)

    def __len__(self):
        return len(self._row)

    def __getattr__(self, attr):
        return getattr(self._row, attr)

    def __getitem__(self, index):
        return self._row[index]

    def __bytes__(self):
        result = b''
        for item in self:
            result += bytes(item)
        return result

    def get_total_bytes(self):
        return sum(len(item) for item in self)


class PixelMap:
    
    WHITE = 255
    PIXELROW_CLS = PixelRow

    def __init__(self,*, pixels=[], image=None):
        """Create an instance representing a 1 or 2-dimensional
        matrix of pixels.
        
        :param image: path to the image file
        :type image: Image

        :returns: PixelMap object
        """
        if not hasattr(pixels, '__iter__'):
            raise ValueError('pixels argument has to be an iterable')
        if image and not isinstance(image, Image):
            raise ValueError('image has to be of type Image, got {}'.format(type(image).__name__))

        if pixels:
                if isinstance(pixels[0], Pixel):
                    self._map = [self.PIXELROW_CLS(pixels)]
                elif isinstance(pixels[0], (PixelRow, list, tuple)):
                    self._map = [self.PIXELROW_CLS(item) for item in pixels]

        elif not image:
            self._map = []

        else:
            if not hasattr(image, 'body_start'):
                color = image.__dict__.get('color') or Pixel.WHITE
                data = self.make_single_color_data(image.width, image.height, image.depth, color)
            else:
                data = image.data[image.body_start:]

            self._map = self._bytes_to_pixel_map(data, image)
        
    
    def make_single_color_data(self, width, height, depth, color):
        num_pixels = width * height
        num_bytes = depth // 8
        pixel = Pixel([color] * num_bytes)
        matrix = num_pixels * pixel._pixel
        return matrix

    def __repr__(self):
        return "{} {}".format(self.__class__.__name__, self._map)

    def __getitem__(self, index):
        if isinstance(index, slice):
            return self.__class__(pixels=self._map[index])
        return self._map[index]

    def __setitem__(self, index, value):
        if not isinstance(value, self.PIXELROW_CLS):
            raise ValueError('Expected PixelRow object')
        self._map[index] = value

    def __getattr__(self, attr):
        return getattr(self._map, attr)

    def __bytes__(self):
        result = b''
        for item in self:
            result += bytes(item)
        return result

    @property
    def size(self):
        return self.width, self.height

    @property
    def height(self):
        '''Counts the number of rows'''
        return len(self._map)

    @property
    def width(self):
        '''Counts the number of pixels'''
        return len(self._map[0])
    
    def _bytes_to_pixel_map(self, data, image):
        """Generate new PixelMap object from a sequence of bytes"""

        if isinstance(data, (bytes, bytearray)):
            off = 0
            step = image.depth // 8

            pixel_map = []
            for row_num in range(image.height):
                pixels = []
                for col_num in range(image.width):
                    
                    pixel = data[off: off + step]
                    off += step
                    
                    pixels.append(self.PIXELROW_CLS.PIXEL_CLS(pixel))

                pixel_map.append(self.PIXELROW_CLS(pixels))

            return pixel_map

        raise ValueError('Data has to be bytes not {}'.format(type(data).__name__))


class Image:
    """Class to represent an image object. """

    PIXELMAP_CLS = PixelMap

    def __init__(self, *, filepath, data=None, **kwargs):
        """Parses the file bytes into image object or generates new data for
        non-existent file.
        
        :param filepath: path to the image file
        :type filepath: string
        :data: bytes string representing image data including the header
        :type filepath: bytes

        :returns: Image object
        """
        self.name = filepath

        if data:
            self.data = data
            self.depth = self._get_metadata('Depth')
            self.body_start = self._get_metadata('Pixel_array_offset')

            self.size = (
                self._get_metadata('Width'), 
                self._get_metadata('Height')
            )

            self.body = self.PIXEL_MAP_CLS(image=self)
            self.horz_res = self._get_metadata('Horizont_res')
            self.vert_res = self._get_metadata('Vertical_res')

        else:
            try:
                self.size = kwargs['size']
                self.depth = kwargs['depth']
            except KeyError as e:
                raise ValueError('Missing argument: {}', e.args[0])

            self.body = self.PIXEL_MAP_CLS(image=self)
            self.body_start = self.HEADER_SIZE
            self.horz_res = 2835
            self.vert_res = 2835
            self.data = self.make_header() + bytes(self.body)


    @property
    def width(self):
        return self.size[0]

    @property
    def height(self):
        return self.size[1]

    @property
    def file_size(self):
        return self.HEADER_SIZE + self.get_img_size()

    def get_img_size(self):
        return self.width * self.height * (self.depth // 8)

    def _get_metadata(self, prop):
        raise NotImplementedError

    def make_header(self):
        raise NotImplementedError


    def save(self):
        """Requests a new header to be generated and joined with the image data
        and writes the data to a newly created file.

        :param self: image object
        """
        with open(self.name, 'wb') as file:
            self.data = self.make_header() + bytes(self.body)
            file.write(self.data)

    def copy(self):
        base, ext = os.path.splitext(self.name)
        filepath = '{}_{}{}'.format(
                        base, 
                        dt.now().strftime('%Y_%m_%d-%H_%M_%S'),
                        ext
                    )
        copied = self.__class__(filepath=filepath, data=self.data)

        return copied

    def crop(self, start, end):
        '''Returns a rectangular region cout out from an image. 
        The region is defined by start and end coordinates.

        :param start: coordinates referring to the left-upper corner of the crop rectangle
        :type start: two item iterable of integers
        :param end: coordinates referring to the right-lower corner of the crop rectangle
        :type end: two item iterable of integers

        returns: new image object
        '''
        cropped = self.copy()
        if not start and end: 
            return cropped

        start_row = start[0]
        start_col = start[1]
        end_row = end[0] + 1
        end_col = end[1] + 1

        width = end_col - start_col
        height = end_row - start_row
        cropped.size = (width, height)

        # starting with the header
        cropped.data = self.data[:self.HEADER_SIZE]
        cropped.body = self.PIXELMAP_CLS()

        for row in self.body[start_row:end_row]:
            pixel_row = self.PIXELMAP_CLS.PIXELROW_CLS(row[start_col: end_col])
            cropped.body.append(pixel_row)
            cropped.data +=  bytes(pixel_row) 

        return cropped