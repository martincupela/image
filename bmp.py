import io

from .base import PixelMap, PixelRow, Image
from .utils import *


class BmpPixelRow(PixelRow):

    def __init__(self, *args):
        super().__init__(*args)
        if len(self) > 0:
            self.add_padding_bytes()

    def add_padding_bytes(self):
        bytes_in_row = self.get_total_bytes()
        if bytes_in_row % 4 != 0:
            padding_bytes =  bin8(0) * (4 - (bytes_in_row % 4))
            self._row.append(padding_bytes)


class BmpPixelMap(PixelMap):

    PIXELROW_CLS = BmpPixelRow

    def __getitem__(self, index):
        index = self.invert_slice(index)
        return super().__getitem__(index)


    def __setitem__(self, index, value):
        index = self.invert_slice(index)
        super().__setitem__(index, value)

    def invert_slice(self, index): 
        '''Inverts the index or slice numbers if object being retrieved is
        a pixel map - row in a pixel map. Always returns a positive integer'''
        
        if isinstance(index, int):
            return self.height - abs(index) - 1

        elif isinstance(index, slice):
            if index.start is None:
                start = -1
            else:
                start = -(index.start + 1)

            if index.stop is None:
                stop = - (self.height+ 1)
            else:
                stop = - (index.stop + 1)

            if index.step is None:
                step = -1 if start > stop else 1
            else:
                step = -index.step

            return slice(start, stop, step)

    def _bytes_to_pixel_map(self, data, image):
        """Generate new PixelMap object from a sequence of bytes"""
        if isinstance(data, (bytes, bytearray)):
            # each row has to have the same length
            
            off = 0
            step = image.depth // 8
            num_padding_bytes = 0 if image.width % 4 == 0 else 4 - (image.width % 4) 

            pixel_map = []
            for row_num in range(image.height):
                pixels = []
                for col_num in range(image.width):
                    
                    pixel = data[off: off + step]
                    off += step
                    
                    pixels.append(self.PIXELROW_CLS.PIXEL_CLS(pixel))

                pixel_map.append(self.PIXELROW_CLS(pixels))
                off += num_padding_bytes                  

            return pixel_map

        raise ValueError('Data has to be bytes not {}'.format(type(data).__name__))

class BmpImage(Image):

    HEADER_FIELDS = {
        'Size':               ((2, 6),   lambda obj: bin32le(obj.file_size)),
        'Pixel_array_offset': ((10, 14), lambda obj: bin32le(54)),
        'DIB_Header_Size':    ((14, 18), lambda obj: bin32le(40)),
        'Width':              ((18, 22), lambda obj: bin32le(obj.width)),
        'Height':             ((22, 26), lambda obj: bin32le(obj.height)),
        'Color_planes_num':   ((26, 28), lambda obj: bin16le(1)),
        'Depth':              ((28, 30), lambda obj: bin16le(obj.depth)),
        'Img_size':           ((34, 38), lambda obj: bin32le(obj.get_img_size())),
        'Horizont_res':       ((38, 42), lambda obj: bin32le(obj.horz_res)),
        'Vertical_res':       ((42, 46), lambda obj: bin32le(obj.vert_res)),
    }

    SIGNATURE = b'BM'
    HEADER_SIZE = 54
    PIXEL_MAP_CLS = BmpPixelMap

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.format = 'bmp'


    def _get_metadata(self, prop):
        '''Extract the image metadata from the file bytes'''

        start, stop = self.HEADER_FIELDS[prop][0]
        if stop - start == 2:
            return int16le(self.data[start: stop])
        elif stop - start == 4:
            return int32le(self.data[start: stop])

    def make_header(self):
        """Creates a bytes string representing image file's header
        by using the functions defined in HEADER_FIELDS dict
        for bmp image format
        """
        with io.BytesIO() as buffr:
            buffr.write(self.HEADER_SIZE * b'\x00')
            buffr.seek(0)
            buffr.write(self.SIGNATURE)

            for offset, func in self.HEADER_FIELDS.values():
                buffr.seek(offset[0])
                buffr.write(func(self))

            buffr.seek(0)
            return buffr.read()
