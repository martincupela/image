class UnsupportedFormatError(Exception):
    pass

class FileNameError(Exception):
    pass