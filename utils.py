from struct import unpack, pack

def bin8(number):
    return bytes((number, ))

def bin16le(number):
    return pack("<H", number)


def bin32le(number):
    return pack("<I", number)

def int16le(byte_string, offset=0):
    return unpack("<H", byte_string[offset: offset + 2])[0]


def int32le(byte_string, offset=0):
    return unpack("<I", byte_string[offset: offset + 4])[0]